<?php

include_once("Book.php");
include_once("IModel.php");

/** The Model is the class holding data about a collection of books.
 * @author Rune Hjelsvold
 * @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
 */

class DBModel implements IModel
{
	 protected $db = null;

	public function __construct($db = null)
    {
		$servername = "localhost";
		$username = "root";
		$password = "";

		if($db){
			$this->db = $db;
		} else {
				try {
						$connection = new PDO("mysql:host=$servername;dbname=test;charset=utf8mb4", $username, $password);
						// set the PDO error mode to exception
						$connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
						$connection->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
						$this->db = $connection;
						//echo "Connected successfully";
					}
				catch(PDOException $e)
					{
						echo "Connection failed<br>";
					}
			}
	}

	/** Function returning the complete list of books in the collection. Books are
	 * returned in order of id.
	 * @return Book[] An array of book objects indexed and ordered by their id.
	 */

	public function getBookList() {
		$books = array();
			foreach($this->db->query('SELECT * FROM book', PDO::FETCH_ASSOC) as $row) {
				$book = new Book($row['title'],$row['author'], $row['description'], $row['id']);
				array_push($books, $book);
			}
		return $books;
	}

	/** Function retrieveing information about a given book in the collection.
	 * @param integer $id The id of the book to be retrieved.
	 * @return Book|null The book matching the $id exists in the collection; null otherwise.
	 */

	public function getBookById($id)
	{

		if(is_numeric($id)){
			$idx = $this->getBookIndexById($id);

			if ($idx > -1)
			{
			$theBook = $this->db->prepare(
					"SELECT *
					FROM book
					WHERE id = $id");

					$theBook->execute();
					$result = $theBook->fetch();

					$book = new Book($result['title'],$result['author'], $result['description'], $result['id']);
				return $book;
			}
		}
 		return null;
	}

	/** Adds a new book to the collection.
	 * @param $book Book The book to be added - the id of the book will be set after successful insertion.
	 * @return 1 = successful , 2 = PDOException, 3 = failed author or/and title
	 */
	public function addBook($book)
	{
		if($book->title != null && $book->author != null && $book->title != "" && $book->author != "") {
			try{
	    $book->id = $this->nextId();
		 	$newRow = $this->db->prepare("INSERT INTO book (id, title, author, description)
        VALUES (:id, :title, :author, :description)");
			$newRow->bindValue(':id', $book->id, PDO::PARAM_INT);
			$newRow->bindParam(':title', $book->title, PDO::PARAM_STR , 500);
			$newRow->bindParam(':author', $book->author, PDO::PARAM_STR, 250);
			$newRow->bindParam(':description',$book->description,
							empty($book->description) ? PDO::PARAM_NULL : PDO::PARAM_STR, 2000);

			$newRow->execute();
			return 1;	// successful
		}
		catch(PDOException $e)
			{
				return 3;	// PDO
			}
	}
	return 2;	// failed author or/and title
}

	/** Modifies data related to a book in the collection.
	 * @param Book $book The book data to kept.
	 * @return 1 = successful , 2 = PDOException, 3 = failed to find idx
	 */
	public function modifyBook($book)
	{
		$idx = $this->getBookIndexById($book->id);
		if ($idx > -1)
		{
		try{
				$update = "UPDATE book SET title=:title, author=:author, description=:description WHERE id=:id";
				$stmt = $this->db->prepare($update);
        $stmt->bindValue(':id', $book->id, PDO::PARAM_INT);
        $stmt->bindParam(':title', $book->title, PDO::PARAM_STR, 500);
        $stmt->bindParam(':author', $book->author, PDO::PARAM_STR, 250);
        $stmt->bindParam(':description', $book->description,
                          empty($book->description) ? PDO::PARAM_NULL : PDO::PARAM_STR, 2000);
			 $stmt->execute();
			 return 1; // successful
			}
		catch(PDOException $e)
			{
			return 3; 	// PDO
			}
		}
		return 4;	// failed too find idx
	}

	/** Deletes data related to a book from the collection.
	 * @param integer $id The id of the book that should be removed from the collection.
	 * @return true = deleted book, false = book was not deleted
	 */
	public function deleteBook($id)
	{
		$idx = $this->getBookIndexById($id);
			if ($idx > -1)
			{
				$deleteRow = "DELETE FROM book WHERE id = $id";
				$this->db->exec($deleteRow);
				echo "<br>Book deleted successfully";
				return 1; //successfull
			}
			return 5; //error failed to delete
	}

	/** Helper function finding the location of the book in the collection array.
	 * @param integer $id The id of the book to look for.
	 * @return integer The index of the book in the collection array; -1 if the book is
	 *                 not found in the array.
	 */
	protected function getBookIndexById($id)
	{
		$bookList = $this->getBookList();
		for ($i = 0; $i < sizeof($bookList); $i++)
        {
					if($bookList[$i]->id == $id){
					return $i;
					}
				}
		return -1;
	}

	/** Helper function generating a sequence of ids.
	 * @return integer A value larger than the largest book id in the collection.
	 */
	protected function nextId()
	{
		$maxId = 0;
		$bookList = $this->getBookList();

		foreach ($bookList as $book)
		{
			if (isset($book) && $book->id > $maxId)
			{
				$maxId = $book->id;
			}
		}
		return $maxId + 1;
	}

}

?>
